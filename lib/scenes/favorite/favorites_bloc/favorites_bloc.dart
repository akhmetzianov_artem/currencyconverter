import 'dart:async';

import 'package:currancyconverter/scenes/converter/model/currency.dart';
import 'package:currancyconverter/scenes/favorite/favorites_bloc/workers/currency_fetcher.dart';
import 'package:currancyconverter/scenes/favorite/favorites_bloc/workers/favorite_update_worker.dart';
import 'package:rxdart/rxdart.dart';

class FavoritesBloc {
  final FavoriteCurrencyFetcherService _favoriteCurrencyFetcher;
  final FavoriteUpdateWorker _favoriteUpdateWorker;
  final BehaviorSubject<List<Currency>> _currenciesSubject =
      BehaviorSubject.seeded([]);

  Stream<List<Currency>> get currenciesStream => _currenciesSubject.stream;
  final StreamController<String> _filterController = StreamController();

  StreamSink<String> get filterSink => _filterController.sink;

  List<Currency> _currencies = [];
  List<Currency> _filtered = [];

  String _filter = '';

  FavoritesBloc(this._favoriteCurrencyFetcher, this._favoriteUpdateWorker) {
    _listenCurrencies();
    _listenFilter();
  }

  void _listenCurrencies() {
    _favoriteCurrencyFetcher.currenciesStream.listen((values) {
      _currencies = values;
      _updateCurrenciesToShow();
    });
  }

  void _listenFilter() {
    _filterController.stream.listen((filter) {
      _filter = filter.toLowerCase();
      _updateCurrenciesToShow();
    });
  }

  void _updateCurrenciesToShow() {
    if (_filter.isEmpty) {
      _filtered = _currencies;
    } else {
      _filtered = _currencies.where((currency) {
        return currency.abbreviation.toLowerCase().startsWith(_filter) ||
            currency.name.toLowerCase().startsWith(_filter);
      }).toList();
    }
    _currenciesSubject.add(_filtered);
  }

  void onCurrencyPicked(Currency currency, bool newValue) async {
    await _favoriteUpdateWorker.updateFavorite(currency.abbreviation, newValue);
    _favoriteCurrencyFetcher.updateCurrencies();
  }
}
