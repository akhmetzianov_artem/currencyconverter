import 'package:currancyconverter/shared_workers/shared_preferences_helper.dart';

abstract class FavoriteUpdateWorker {
  Future<void> updateFavorite(String abbrev, bool isFavorite);
}

class FavoriteUpdater implements FavoriteUpdateWorker {
  final SharedPreferencesService _sharedPreferencesService;

  FavoriteUpdater(this._sharedPreferencesService);

  @override
  Future<void> updateFavorite(String abbrev, bool isFavorite) async {
    var favorites = await _sharedPreferencesService.getFavorite();
    if (isFavorite) {
      favorites.add(abbrev);
    } else {
      favorites.remove(abbrev);
    }
    await _sharedPreferencesService.setFavorite(favorites);
  }
}
