import 'dart:async';

import 'package:currancyconverter/network/api/currency_api/model/currency.dart';
import 'package:currancyconverter/repo/currency_repo/currency_repo.dart';
import 'package:currancyconverter/scenes/converter/model/currency.dart';
import 'package:currancyconverter/shared_workers/shared_preferences_helper.dart';
import 'package:rxdart/rxdart.dart';

abstract class FavoriteCurrencyFetcherService {
  Stream<List<Currency>> get currenciesStream;

  void updateCurrencies();
}

class FavoriteCurrencyFetcher implements FavoriteCurrencyFetcherService {
  final CurrencyRepo _currencyRepo;
  final SharedPreferencesService _sharedPreferences;
  final BehaviorSubject<List<Currency>> _currenciesSubject =
      BehaviorSubject.seeded([]);

  FavoriteCurrencyFetcher(this._currencyRepo, this._sharedPreferences) {
    _listenCurrencies();
  }

  Stream<List<Currency>> get currenciesStream => _currenciesSubject.stream;

  void _listenCurrencies() {
    _currencyRepo.currenciesStream.listen((currencies) {
      _handleCurrencies(currencies);
    });
  }

  void _handleCurrencies(List<ApiCurrency> currencies) async {
    var favoriteAbbrevs = await _sharedPreferences.getFavorite();
    var favorites = currencies
        .map((currency) => Currency(
            name: currency.name,
            abbreviation: currency.abbreviation,
            rate: currency.rate,
            isFavorite: favoriteAbbrevs.contains(currency.abbreviation)))
        .toList();
    favorites.add(Currency(
      name: "Українська гривня",
      rate: 1,
      abbreviation: "UAH",
      isFavorite: true,
    ));
    _currenciesSubject.add(favorites);
  }

  @override
  void updateCurrencies() {
    _handleCurrencies(_currencyRepo.apiCurrencies);
  }
}
