import 'package:currancyconverter/app_state.dart';
import 'package:currancyconverter/app_theme.dart';
import 'package:currancyconverter/scenes/converter/model/currency.dart';
import 'package:currancyconverter/scenes/currency_picker/components/custom_text_field.dart';
import 'package:currancyconverter/scenes/favorite/favorites_bloc/favorites_bloc.dart';
import 'package:flutter/material.dart';

class FavoritesPicker extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var _bloc = AppContainerState.of(context).blocProvider.favoritesBloc;
    return Scaffold(
      backgroundColor: AppTheme.primaryColor,
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  IconButton(
                    icon: Icon(
                      Icons.arrow_back,
                      color: Colors.white,
                    ),
                    onPressed: () => Navigator.of(context).pop(),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: CustomTextField(
                        underlineColor: Colors.white,
                        underlineFocusedColor: Colors.white,
                        suffixIcon: Icon(
                          Icons.search,
                          color: Colors.white,
                        ),
                        onChanged: (value) => _bloc.filterSink.add(value),
                        style: Theme.of(context)
                            .textTheme
                            .body2
                            .copyWith(color: Colors.white, fontSize: 25),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 16.0),
              Expanded(
                child: StreamBuilder<List<Currency>>(
                    initialData: [],
                    stream: _bloc.currenciesStream,
                    builder: (context, snapshot) {
                      return ListView.separated(
                          shrinkWrap: true,
                          itemBuilder: (context, index) {
                            return _buildCheckboxTile(
                                context, _bloc, snapshot.data[index]);
                          },
                          separatorBuilder: (context, index) {
                            return Divider(
                              height: 1.0,
                              color: Colors.white,
                              indent: 4.0,
                            );
                          },
                          itemCount: snapshot.data.length);
                    }),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildCheckboxTile(
      BuildContext context, FavoritesBloc bloc, Currency currency) {
    return CheckboxListTile(
      key: ValueKey(currency.isFavorite),
      dense: false,
      checkColor: Colors.white,
      activeColor: AppTheme.primaryColor,
      title: Text(
        currency.abbreviation,
        style: Theme.of(context)
            .textTheme
            .body1
            .copyWith(color: Colors.white, fontSize: 24),
      ),
      subtitle: Text(currency.name,
          style:
              Theme.of(context).textTheme.body2.copyWith(color: Colors.white)),
      value: currency.isFavorite,
      onChanged: (bool value) => bloc.onCurrencyPicked(currency, value),
    );
  }
}
