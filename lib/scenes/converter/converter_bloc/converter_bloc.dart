import 'dart:async';

import 'package:currancyconverter/scenes/converter/converter_bloc/workers/currency_converter.dart';
import 'package:currancyconverter/scenes/converter/converter_bloc/workers/currency_fetcher.dart';
import 'package:currancyconverter/scenes/converter/converter_bloc/workers/number_formatter.dart';
import 'package:currancyconverter/scenes/converter/model/currency.dart';
import 'package:currancyconverter/scenes/currency_picker/components/body.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

enum CurrencyType { BASE, QUOTE }

class ConverterBloc {
  final BehaviorSubject<String> _baseInputAmountController =
      BehaviorSubject();
  final BehaviorSubject<String> _baseOutputAmountController =
      BehaviorSubject();
  final BehaviorSubject<String> _quoteAmountController =
      BehaviorSubject();
  final BehaviorSubject<String> _baseCurrencyController =
      BehaviorSubject();
  final BehaviorSubject<String> _quoteCurrencyController =
      BehaviorSubject();
  final BehaviorSubject<String> _baseAbbrevController =
      BehaviorSubject();
  final BehaviorSubject<String> _quoteAbbrevController =
      BehaviorSubject();

  final BehaviorSubject<bool> _loadingController = BehaviorSubject();
  Stream<bool> get loadingStream => _loadingController.stream;

  final CurrencyConverterWorker _converterWorker;
  final CurrencyFetcherService _currencyFetcher;
  final NumberFormatterService _numberFormatter;

  ConverterBloc(
      this._converterWorker, this._currencyFetcher, this._numberFormatter) {
    _listenBaseAmount();
    _listenCurrencies();
    _listenLoading();
  }

  Stream<String> get _baseInputAmountStream =>
      _baseInputAmountController.stream;

  StreamSink<String> get baseInputAmountSink => _baseInputAmountController.sink;

  Stream<String> get baseOutputAmountStream =>
      _baseOutputAmountController.stream;

  Stream<String> get quoteAmountStream => _quoteAmountController.stream;

  Stream<String> get baseCurrencyStream => _baseCurrencyController.stream;

  Stream<String> get quoteCurrencyStream => _quoteCurrencyController.stream;

  Stream<String> get baseAbbrevStream => _baseAbbrevController.stream;

  Stream<String> get quoteAbbrevStream => _quoteAbbrevController.stream;

  List<Currency> _currencies = [];
  Currency _baseCurrency;
  Currency _quoteCurrency;
  String _baseAmount = "0";
  String _quoteAmount = "0";
  int _maxCharacters = 8;

  void swapCurrencies() {
    var temp = _baseCurrency;
    _baseCurrency = _quoteCurrency;
    _quoteCurrency = temp;
    _updateAmounts();
  }

  void updateCurrencies() {
    _currencyFetcher.updateCurrencies();
  }

  void _listenBaseAmount() {
    _baseInputAmountStream.listen(_onBaseAmountChanged);
  }

  void _listenCurrencies() {
    _currencyFetcher.currenciesStream.listen((currencies) {
      _handleCurrencies(currencies);
    });
  }

  void _listenLoading() {
    _currencyFetcher.loadingStream.listen((isLoading) {
      _loadingController.add(isLoading);
    });
  }

  void _handleCurrencies(List<Currency> currencies) {
    _currencies = currencies;
    if (currencies.length > 1) {
      if (_baseCurrency == null || !currencies.contains(_baseCurrency)) {
        _setDefaultCurrency(CurrencyType.BASE);
      }
      if (_quoteCurrency == null || !currencies.contains(_quoteCurrency)) {
        _setDefaultCurrency(CurrencyType.QUOTE);
      }
      _onCurrencyChanged();
    }
  }

  void _setDefaultCurrency(CurrencyType type) {
    switch (type) {
      case CurrencyType.BASE:
        _baseCurrency = _currencies
            .firstWhere((currency) => currency.abbreviation == "USD");
        break;
      case CurrencyType.QUOTE:
        _quoteCurrency = _currencies
            .firstWhere((currency) => currency.abbreviation == "UAH");
        break;
    }
  }

  void _onCurrencyChanged() {
    _baseCurrencyController.add(_baseCurrency.name);
    _quoteCurrencyController.add(_quoteCurrency.name);
    _baseOutputAmountController.add(_baseAmount);
    _quoteAmountController.add(_quoteAmount);
    _baseAbbrevController.add(_baseCurrency.abbreviation);
    _quoteAbbrevController.add(_quoteCurrency.abbreviation);
  }

  void _onBaseAmountChanged(String value) {
    try {
      var _ = int.parse(value);
      if (_baseAmount.length < _maxCharacters) {
        handleNumber(value);
        _updateAmounts();
      }
    } catch (e) {
      if (value == 'back') {
        _handleBack();
      } else {
        _handleDot();
      }
      _updateAmounts();
    }
  }

  void handleNumber(String number) {
    _baseAmount = _baseAmount == "0" ? number : _baseAmount + number;
  }

  void _handleBack() {
    if (_baseAmount.length == 1) {
      _baseAmount = '0';
    } else {
      _baseAmount = _baseAmount.substring(0, _baseAmount.length - 1);
    }
  }

  void _handleDot() {
    if (!_baseAmount.contains('.')) {
      _baseAmount += '.';
    }
  }

  void _updateAmounts() {
    var baseAmountValue = _numberFormatter.toDouble(_baseAmount);
    double quoteAmountValue = _converterWorker.convertAmount(
        baseAmountValue, _baseCurrency, _quoteCurrency);
    _quoteAmount = _numberFormatter.fromDouble(quoteAmountValue);
    _onCurrencyChanged();
  }

  void onCurrencyPicked(CurrencyType type, BuildContext context) async {
    var currency = await showGeneralDialog(
        context: context,
        barrierLabel: "",
        barrierDismissible: true,
        barrierColor: Colors.white.withOpacity(0.5),
        transitionDuration: Duration(milliseconds: 300),
        transitionBuilder: (context, animation, secondAnimation, child) {
          animation = CurvedAnimation(parent: animation, curve: Curves.ease);
          return ScaleTransition(
            scale: animation,
            child: child,
          );
        },
        pageBuilder: (context, animation, secondAnimation) {
          return AlertDialog(
              backgroundColor: Colors.transparent, content: CurrencyPicker());
        }) as Currency;
    if (currency == null) return;

    switch (type) {
      case CurrencyType.BASE:
        _baseCurrency = currency;
        break;
      case CurrencyType.QUOTE:
        _quoteCurrency = currency;
        break;
    }
    _updateAmounts();
  }
}
