import 'dart:async';

import 'package:currancyconverter/network/api/currency_api/model/currency.dart';
import 'package:currancyconverter/repo/currency_repo/currency_repo.dart';
import 'package:currancyconverter/shared_workers/shared_preferences_helper.dart';
import 'package:currancyconverter/scenes/converter/model/currency.dart';
import 'package:rxdart/rxdart.dart';

abstract class CurrencyFetcherService {
  Stream<List<Currency>> get currenciesStream;

  Stream<bool> get loadingStream;

  void updateCurrencies();
}

class CurrencyFetcher implements CurrencyFetcherService {
  final BehaviorSubject<List<Currency>> _currencyController = BehaviorSubject();
  final BehaviorSubject<bool> _loadingController = BehaviorSubject();

  final CurrencyRepo _currencyRepo;
  final SharedPreferencesService _preferencesService;
  final List<String> _defaultFavorite = ["USD", "EUR", "UAH"];
  final _uahCurrency = Currency(
    name: "Українська гривня",
    rate: 1,
    abbreviation: "UAH",
    isFavorite: true,
  );

  CurrencyFetcher(this._currencyRepo, this._preferencesService) {
    _subscribeToCurrencies();
    _subscribeToLoading();
  }

  @override
  Stream<List<Currency>> get currenciesStream => _currencyController.stream;

  @override
  Stream<bool> get loadingStream => _loadingController.stream;

  void _subscribeToCurrencies() {
    _currencyRepo.currenciesStream.listen((currencies) {
      _handleApiCurrencies(currencies);
    });
  }

  void _subscribeToLoading() {
    _currencyRepo.loadingStream
        .listen((isLoading) => _loadingController.add(isLoading));
  }

  Future<void> _handleApiCurrencies(List<ApiCurrency> apiCurrencies) async {
    var favorite = await _getFavoriteCurrencies();
    var currencies = apiCurrencies.map((apiCurrency) {
      var isFavorite = favorite.contains(apiCurrency.abbreviation);
      return Currency(
          name: apiCurrency.name,
          rate: apiCurrency.rate,
          abbreviation: apiCurrency.abbreviation,
          isFavorite: isFavorite);
    }).toList();
    currencies.add(_uahCurrency);
    _currencyController.add(currencies);
  }

  Future<List<String>> _getFavoriteCurrencies() async {
    var favorite = await _preferencesService.getFavorite();
    if (favorite.isEmpty) {
      favorite = _defaultFavorite;
      await _preferencesService.setFavorite(favorite);
    }
    return favorite;
  }

  @override
  void updateCurrencies() {
    _currencyRepo.updateCurrencies();
  }
}
