abstract class NumberFormatterService {
  String fromDouble(double value);

  double toDouble(String value);
}

class NumberFormatter implements NumberFormatterService {
  @override
  String fromDouble(double value) {
    var formattedValue = value.toStringAsFixed(2);
    var splitNumber = formattedValue.split('.');
    return splitNumber[1] == '00' ? splitNumber[0] : formattedValue;
  }

  @override
  double toDouble(String value) {
    return double.parse(value);
  }
}
