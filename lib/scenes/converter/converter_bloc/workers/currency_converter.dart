import 'package:currancyconverter/scenes/converter/model/currency.dart';

abstract class CurrencyConverterWorker {
  double convertAmount(
      double baseAmount, Currency baseCurrency, Currency quoteCurrency);
}

class CurrencyConverter implements CurrencyConverterWorker {
  @override
  double convertAmount(double baseAmount, Currency baseCurrency, Currency quoteCurrency) {
    if(baseAmount == 0) return 0;
    var baseInUah = baseCurrency.rate * baseAmount;
    return (baseInUah / quoteCurrency.rate);
  }
}
