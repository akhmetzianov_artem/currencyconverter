import 'package:flutter/cupertino.dart';

class Currency {
  final String name;
  final String abbreviation;
  final double rate;
  bool isFavorite;

  Currency(
      {@required this.name,
      @required this.abbreviation,
      @required this.rate,
      @required this.isFavorite});
}
