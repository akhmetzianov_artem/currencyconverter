import 'package:flutter/material.dart';

import '../../../../app_theme.dart';

class KeyModel {
  Widget title;
  String value;
  Color backgroundColor;

  KeyModel(this.title, this.value, this.backgroundColor);

  KeyModel.withValue(KeyValue keyValue) {
    Color backgroundColor = AppTheme.keyboardKeyColor;
    String value;
    Widget title;
    switch (keyValue) {
      case KeyValue.ONE:
        value = "1";
        break;
      case KeyValue.TWO:
        value = "2";
        break;
      case KeyValue.THREE:
        value = "3";
        break;
      case KeyValue.FOUR:
        value = "4";
        break;
      case KeyValue.FIVE:
        value = "5";
        break;
      case KeyValue.SIX:
        value = "6";
        break;
      case KeyValue.SEVEN:
        value = "7";
        break;
      case KeyValue.EIGHT:
        value = "8";
        break;
      case KeyValue.NINE:
        value = "9";
        break;
      case KeyValue.ZERO:
        value = "0";
        break;
      case KeyValue.DOT:
        value = ".";
        backgroundColor = Colors.transparent;
        break;
      case KeyValue.BACK:
        value = "back";
        backgroundColor = Colors.transparent;
        break;
    }

    switch (keyValue) {
      case KeyValue.ONE:
      case KeyValue.TWO:
      case KeyValue.THREE:
      case KeyValue.FOUR:
      case KeyValue.FIVE:
      case KeyValue.SIX:
      case KeyValue.SEVEN:
      case KeyValue.EIGHT:
      case KeyValue.NINE:
      case KeyValue.ZERO:
      case KeyValue.DOT:
        title = Text(
          value,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 35
          ),
        );
        break;

      case KeyValue.BACK:
        title = Icon(Icons.backspace, color: Colors.white);
    }

    this.title = title;
    this.value = value;
    this.backgroundColor = backgroundColor;
  }
}

enum KeyValue {
  ONE,
  TWO,
  THREE,
  FOUR,
  FIVE,
  SIX,
  SEVEN,
  EIGHT,
  NINE,
  ZERO,
  DOT,
  BACK
}
