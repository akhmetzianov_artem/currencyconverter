import 'package:currancyconverter/app_theme.dart';
import 'package:flutter/material.dart';

import 'key_model.dart';

class Keyboard extends StatelessWidget {
  final Function(String) onClick;
  final List<KeyModel> keys;

  const Keyboard({Key key, this.onClick, this.keys}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: AppTheme.secondaryColor),
      padding: const EdgeInsets.all(8.0),
      child: GridView.count(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        crossAxisCount: 3,
        childAspectRatio: 1.45,
        mainAxisSpacing: 4,
        crossAxisSpacing: 4,
        children: this._getKeys(context),
      ),
    );
  }

  List<Widget> _getKeys(BuildContext context) {
    List<Widget> keys = [];
    this.keys.forEach((model) => {
          keys.add(
            GestureDetector(
              onTap: () => {onClick(model.value)},
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4.0),
                  color: model.backgroundColor,
                ),
                child: Center(child: model.title),
              ),
            ),
          )
        });
    return keys;
  }
}
