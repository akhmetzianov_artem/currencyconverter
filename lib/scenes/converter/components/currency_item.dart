import 'package:auto_size_text/auto_size_text.dart';
import 'package:currancyconverter/app_theme.dart';
import 'package:flutter/material.dart';

class CurrencyItem extends StatelessWidget {
  final Color backgroundColor;
  final Stream<String> currencyNameStream;
  final Stream<String> amountStream;
  final Stream<String> abbrevStream;
  final VoidCallback onCurrencyTapped;

  CurrencyItem(
      {@required this.backgroundColor,
      @required this.currencyNameStream,
      @required this.amountStream,
      @required this.abbrevStream,
      @required this.onCurrencyTapped});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.2,
      decoration: BoxDecoration(color: this.backgroundColor),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
                flex: 1,
                child: GestureDetector(
                  onTap: (() {
                    this.onCurrencyTapped();
                  }),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 16.0),
                    child: Card(
                      margin: EdgeInsets.symmetric(vertical: 24.0),
                      color: backgroundColor,
                      elevation: 4.0,
                      child: Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            _getAbbreviationWidget(),
                            _getCurrencyNameWidget()
                          ],
                        ),
                      ),
                    ),
                  ),
                )),
            Expanded(
              flex: 2,
              child: StreamBuilder<String>(
                  initialData: '',
                  stream: amountStream,
                  builder: (context, snapshot) {
                    return Padding(
                      padding: const EdgeInsets.only(right: 16.0),
                      child: AutoSizeText(
                        snapshot.data,
                        maxLines: 1,
                        minFontSize: AppTheme.minTitleFontSize,
                        textAlign: TextAlign.right,
                        style: Theme.of(context).textTheme.title,
                      ),
                    );
                  }),
            )
          ]),
    );
  }

  Widget _getAbbreviationWidget() {
    return StreamBuilder(
      initialData: '',
      stream: abbrevStream,
      builder: ((context, snapshot) {
        return Text(snapshot.data, style: Theme.of(context).textTheme.body1);
      }),
    );
  }

  Widget _getCurrencyNameWidget() {
    return StreamBuilder(
        initialData: '',
        stream: currencyNameStream,
        builder: ((context, snapshot) {
          return AutoSizeText(snapshot.data,
              style: Theme.of(context).textTheme.body2,
            minFontSize: 14,
            maxLines: 2,
          );
        }));
  }
}
