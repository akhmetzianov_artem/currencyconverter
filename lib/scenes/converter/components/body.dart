import 'package:currancyconverter/app_state.dart';
import 'package:currancyconverter/app_theme.dart';
import 'package:currancyconverter/scenes/converter/converter_bloc/converter_bloc.dart';
import 'package:currancyconverter/scenes/favorite/components/body.dart';
import 'package:flutter/material.dart';

import 'currency_item.dart';
import 'keyboard/key_model.dart';
import 'keyboard/keyboard.dart';

class Converter extends StatefulWidget {
  const Converter({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return ConverterState();
  }
}

class ConverterState extends State<Converter> {
  List<KeyModel> _keys;
  ConverterBloc _bloc;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _bloc = AppContainerState.of(context).blocProvider.converterBloc;
  }

  @override
  void initState() {
    super.initState();

    List<KeyModel> keys = [];
    var keyValues = [
      KeyValue.ONE,
      KeyValue.TWO,
      KeyValue.THREE,
      KeyValue.FOUR,
      KeyValue.FIVE,
      KeyValue.SIX,
      KeyValue.SEVEN,
      KeyValue.EIGHT,
      KeyValue.NINE,
      KeyValue.DOT,
      KeyValue.ZERO,
      KeyValue.BACK
    ];
    keyValues.forEach((keyValue) => {keys.add(KeyModel.withValue(keyValue))});

    this._keys = keys;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text('Converter'),
        iconTheme: IconThemeData(color: Colors.white),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: () => _bloc.updateCurrencies(),
          ),
          IconButton(
            icon: Icon(Icons.star),
            onPressed: () {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => FavoritesPicker()));
            },
          ),
        ],
      ),
      resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: StreamBuilder<bool>(
            initialData: true,
            stream: _bloc.loadingStream,
            builder: (context, snapshot) {
              return snapshot.data ? _buildLoading() : _buildContent();
            }),
      ),
    );
  }

  Widget _buildContent() {
    return Container(
      child: Stack(
        children: [
          Column(children: <Widget>[
            CurrencyItem(
              backgroundColor: AppTheme.primaryColor,
              currencyNameStream: _bloc.baseCurrencyStream,
              abbrevStream: _bloc.baseAbbrevStream,
              amountStream: _bloc.baseOutputAmountStream,
              onCurrencyTapped: (() {
                _bloc.onCurrencyPicked(CurrencyType.BASE, context);
              }),
            ),
            CurrencyItem(
              backgroundColor: AppTheme.secondaryColor,
              currencyNameStream: _bloc.quoteCurrencyStream,
              abbrevStream: _bloc.quoteAbbrevStream,
              amountStream: _bloc.quoteAmountStream,
              onCurrencyTapped: (() {
                _bloc.onCurrencyPicked(CurrencyType.QUOTE, context);
              }),
            ),
            Expanded(
              child: Keyboard(
                onClick: ((str) => _bloc.baseInputAmountSink.add(str)),
                keys: this._keys,
              ),
            )
          ]),
          Positioned(
            left: (MediaQuery.of(context).size.width * 0.3 - 24) / 2,
            top: MediaQuery.of(context).size.height * 0.2 - 26,
            child: Material(
              color: Colors.transparent,
              child: IconButton(
                icon: Icon(
                  Icons.swap_vert,
                  color: Colors.white,
                ),
                iconSize: 36,
                onPressed: () => _bloc.swapCurrencies(),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildLoading() {
    return Container(
      decoration: BoxDecoration(color: AppTheme.primaryColor),
      child: Center(
        child: SizedBox(
          width: 56.0,
          height: 56.0,
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation(Colors.white),
            strokeWidth: 6,
          ),
        ),
      ),
    );
  }
}
