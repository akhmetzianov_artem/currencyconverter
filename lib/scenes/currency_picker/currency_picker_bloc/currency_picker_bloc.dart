import 'dart:async';

import 'package:currancyconverter/scenes/converter/model/currency.dart';
import 'package:currancyconverter/scenes/currency_picker/currency_picker_bloc/workers/favorites_fetcher.dart';
import 'package:rxdart/rxdart.dart';

class CurrencyPickerBloc {
  final FavoritesFetcherService _service;
  List<Currency> _favorites = [];
  String _filter = '';

  final StreamController<String> _filterController = StreamController();

  StreamSink<String> get filterSink => _filterController.sink;

  CurrencyPickerBloc(this._service) {
    _listenFilter();
  }

  final BehaviorSubject<List<Currency>> _favoritesController =
      BehaviorSubject.seeded([]);

  Stream<List<Currency>> get favoritesStream {
    _listenFavorites();
    return _favoritesController.stream;
  }

  void _listenFavorites() {
    this._service.getFavorites().then((favorites) {
      _favorites = favorites;
      _favoritesController.add(_favorites);
    });
  }

  void _listenFilter() {
    _filterController.stream.listen((filter) {
      _filter = filter.toLowerCase();
      _onFilterChanged();
    });
  }

  void _onFilterChanged() {
    if (_filter.isEmpty) {
      _favoritesController.add(_favorites);
    } else {
      var filteredFavorites = _favorites.where((currency) {
        return currency.abbreviation.toLowerCase().startsWith(_filter) ||
            currency.name.toLowerCase().startsWith(_filter);
      }).toList();
      _favoritesController.add(filteredFavorites);
    }
  }
}
