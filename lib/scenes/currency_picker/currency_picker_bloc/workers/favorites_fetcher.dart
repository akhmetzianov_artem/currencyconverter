import 'package:currancyconverter/repo/currency_repo/currency_repo.dart';
import 'package:currancyconverter/shared_workers/shared_preferences_helper.dart';
import 'package:currancyconverter/scenes/converter/model/currency.dart';

abstract class FavoritesFetcherService {
  Future<List<Currency>> getFavorites();
}

class FavoritesFetcher implements FavoritesFetcherService {
  final CurrencyRepo _currencyRepo;
  final SharedPreferencesService _sharedPreferences;

  FavoritesFetcher(this._sharedPreferences, this._currencyRepo);

  @override
  Future<List<Currency>> getFavorites() async {
    var favoriteAbbrevs = await _sharedPreferences.getFavorite();
    var favoriteCurrencies = _currencyRepo.apiCurrencies;
    var favorites = favoriteCurrencies
        .where((currency) => favoriteAbbrevs.contains(currency.abbreviation))
        .map((currency) => Currency(
            name: currency.name,
            abbreviation: currency.abbreviation,
            rate: currency.rate,
            isFavorite: true))
        .toList();
    favorites.add(Currency(
      name: "Українська гривня",
      rate: 1,
      abbreviation: "UAH",
      isFavorite: true,
    ));
    return favorites;
  }
}
