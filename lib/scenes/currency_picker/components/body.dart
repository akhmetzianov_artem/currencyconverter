import 'package:currancyconverter/app_state.dart';
import 'package:currancyconverter/app_theme.dart';
import 'package:currancyconverter/scenes/converter/model/currency.dart';
import 'package:currancyconverter/scenes/currency_picker/components/custom_text_field.dart';
import 'package:currancyconverter/scenes/currency_picker/currency_picker_bloc/currency_picker_bloc.dart';
import 'package:flutter/material.dart';

class CurrencyPicker extends StatefulWidget {
  @override
  _CurrencyPickerState createState() => _CurrencyPickerState();
}

class _CurrencyPickerState extends State<CurrencyPicker> {
  CurrencyPickerBloc _bloc;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _bloc = AppContainerState.of(context).blocProvider.currencyPickerBloc;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.5,
      width: MediaQuery.of(context).size.width * 0.75,
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(8.0)),
      child: Column(
        children: <Widget>[
          Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
              child: CustomTextField(
                underlineColor: Colors.grey,
                underlineFocusedColor: AppTheme.primaryColor,
                suffixIcon: Icon(Icons.search),
                onChanged: (value) => _bloc.filterSink.add(value),
                style: Theme.of(context)
                    .textTheme
                    .body2
                    .copyWith(color: AppTheme.primaryColor, fontSize: 25),
              )),
          Expanded(child: _buildList())
        ],
      ),
    );
  }

  Widget _buildList() {
    return StreamBuilder<List<Currency>>(
        stream: _bloc.favoritesStream,
        builder: (context, snapshot) {
          return ListView.separated(
              separatorBuilder: (context, index) {
                return Divider(
                  indent: 16.0,
                );
              },
              itemCount: snapshot.data?.length ?? 0,
              itemBuilder: (context, index) {
                return ListTile(
                  onTap: () {
                    Navigator.of(context).pop(snapshot.data[index]);
                  },
                  dense: true,
                  subtitle: Text(
                    snapshot.data[index].name,
                    style: Theme.of(context)
                        .textTheme
                        .subtitle
                        .copyWith(color: Colors.grey),
                  ),
                  title: Text(
                    snapshot.data[index].abbreviation,
                    style: Theme.of(context)
                        .textTheme
                        .body2
                        .copyWith(color: AppTheme.primaryColor),
                  ),
                );
              });
        });
  }
}
