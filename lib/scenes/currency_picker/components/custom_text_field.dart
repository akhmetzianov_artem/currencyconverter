import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  final Function(String) onChanged;
  final Icon suffixIcon;
  final TextStyle style;
  final Color underlineColor;
  final Color underlineFocusedColor;

  const CustomTextField(
      {Key key,
      this.onChanged,
      this.suffixIcon,
      this.style,
      this.underlineColor,
      this.underlineFocusedColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      onChanged: ((value) => onChanged(value)),
      decoration: InputDecoration(
        enabledBorder:
            UnderlineInputBorder(borderSide: BorderSide(color: underlineColor)),
        focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: underlineFocusedColor)),
        suffixIcon: suffixIcon,
      ),
      cursorColor: Colors.white,
      style: style,
    );
  }
}
