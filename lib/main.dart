import 'package:currancyconverter/app.dart';
import 'package:currancyconverter/app_state.dart';
import 'package:currancyconverter/bloc_provider.dart';
import 'package:currancyconverter/database.dart';
import 'package:currancyconverter/network/api/currency_api/currency_api.dart';
import 'package:currancyconverter/repo/currency_repo/currency_repo.dart';
import 'package:currancyconverter/scenes/converter/converter_bloc/converter_bloc.dart';
import 'package:currancyconverter/scenes/converter/converter_bloc/workers/currency_converter.dart';
import 'package:currancyconverter/scenes/converter/converter_bloc/workers/currency_fetcher.dart';
import 'package:currancyconverter/scenes/converter/converter_bloc/workers/number_formatter.dart';
import 'package:currancyconverter/scenes/currency_picker/currency_picker_bloc/currency_picker_bloc.dart';
import 'package:currancyconverter/scenes/currency_picker/currency_picker_bloc/workers/favorites_fetcher.dart';
import 'package:currancyconverter/scenes/favorite/favorites_bloc/favorites_bloc.dart';
import 'package:currancyconverter/scenes/favorite/favorites_bloc/workers/currency_fetcher.dart';
import 'package:currancyconverter/scenes/favorite/favorites_bloc/workers/favorite_update_worker.dart';
import 'package:currancyconverter/shared_workers/shared_preferences_helper.dart';
import 'package:flutter/material.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  var currencyApi = CurrencyApi();
  var currencyRepo = CurrencyRepo(currencyApi, DBHelper.helper);
  var sharedPrefs = SharedPreferencesWorker();
  var currencyConverter = CurrencyConverter();
  var currencyFetcher = CurrencyFetcher(currencyRepo, sharedPrefs);
  var numberFormatter = NumberFormatter();
  var converterBloc =
      ConverterBloc(currencyConverter, currencyFetcher, numberFormatter);
  var favoritesFetcher = FavoritesFetcher(sharedPrefs, currencyRepo);
  var currencyPickerBloc = CurrencyPickerBloc(favoritesFetcher);

  var favoritesCurrencyFetcher =
      FavoriteCurrencyFetcher(currencyRepo, sharedPrefs);
  var favoriteUpdateWorker = FavoriteUpdater(sharedPrefs);
  var favoritesBloc =
      FavoritesBloc(favoritesCurrencyFetcher, favoriteUpdateWorker);

  var blocProvider =
      BlocProvider(converterBloc, currencyPickerBloc, favoritesBloc);

  runApp(AppContainer(
    blocProvider: blocProvider,
    child: ConverterApp(),
  ));
}
