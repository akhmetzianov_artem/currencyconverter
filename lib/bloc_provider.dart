import 'package:currancyconverter/scenes/converter/converter_bloc/converter_bloc.dart';
import 'package:currancyconverter/scenes/currency_picker/currency_picker_bloc/currency_picker_bloc.dart';
import 'package:currancyconverter/scenes/favorite/favorites_bloc/favorites_bloc.dart';

class BlocProvider {
  final ConverterBloc converterBloc;
  final CurrencyPickerBloc currencyPickerBloc;
  final FavoritesBloc favoritesBloc;


  BlocProvider(this.converterBloc, this.currencyPickerBloc, this.favoritesBloc);
}