import 'package:shared_preferences/shared_preferences.dart';

abstract class SharedPreferencesService {
  Future<List<String>> getFavorite();
  Future<bool> setFavorite(List<String> favorite);
}

class SharedPreferencesWorker implements SharedPreferencesService {
  final String _favoriteKey = 'favorite';
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  @override
  Future<List<String>> getFavorite() async {
    return (await _prefs).getStringList(_favoriteKey) ?? [];
  }

  @override
  Future<bool> setFavorite(List<String> favorite) async {
    return (await _prefs).setStringList(_favoriteKey, favorite);
  }
}