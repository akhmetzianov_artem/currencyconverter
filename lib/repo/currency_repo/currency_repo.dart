import 'dart:async';

import 'package:currancyconverter/database.dart';
import 'package:currancyconverter/network/api/currency_api/currency_api.dart';
import 'package:currancyconverter/network/api/currency_api/model/currency.dart';
import 'package:rxdart/rxdart.dart';

class CurrencyRepo {
  final CurrencyApi _currencyApi;
  final DBHelperService _dbHelper;
  final BehaviorSubject<List<ApiCurrency>> _currencySubject =
      BehaviorSubject.seeded([]);
  final BehaviorSubject<bool> _loadingController = BehaviorSubject.seeded(true);

  Stream<bool> get loadingStream => _loadingController.stream;

  Stream<List<ApiCurrency>> get currenciesStream {
    return _currencySubject.stream;
  }

  List<ApiCurrency> get apiCurrencies => _currencySubject.value;

  CurrencyRepo(this._currencyApi, this._dbHelper) {
    updateCurrencies();
  }

  void updateCurrencies() {
    _loadingController.add(true);
    _currencyApi.getCurrencies().asStream().listen((currencies) {
      _loadingController.add(false);
      _synchronizeWithDb(currencies);
    }).onError((error) {
      _loadingController.add(false);
      print(error);
      _fetchFromDb();
    });
  }

  void _synchronizeWithDb(List<ApiCurrency> currencies) {
    _dbHelper.cleanCurrencies();
    _dbHelper.addCurrencies(currencies);
    _currencySubject.add(currencies);
  }

  void _fetchFromDb() async {
    var dbCurrencies = await _dbHelper.getCurrencies();
    _currencySubject.add(dbCurrencies);
  }
}
