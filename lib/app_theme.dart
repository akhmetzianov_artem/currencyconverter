import 'package:flutter/material.dart';

class AppTheme {
  static ThemeData themeData = ThemeData(
      primaryColor: AppTheme.primaryColor,
      primarySwatch: Colors.blue,
      textTheme: TextTheme(
          title: TextStyle(
              fontFamily: AppTheme.customFont,
              color: Colors.white,
              fontSize: AppTheme.maxTitleFontSize),
          body1: TextStyle(
              fontFamily: AppTheme.customFont,
              color: Colors.white,
              fontSize: 30),
          body2: TextStyle(
              fontFamily: AppTheme.customFont,
              color: Colors.white,
              fontSize: 16)));

  static Color primaryColor = Colors.blueAccent;
  static Color secondaryColor = Colors.blueAccent.withOpacity(0.85);
  static Color keyboardKeyColor = Colors.white.withOpacity(0.15);

  static String customFont = 'try-clothing-light.ttf';

  static double maxTitleFontSize = 45.0;
  static double minTitleFontSize = 40.0;
}
