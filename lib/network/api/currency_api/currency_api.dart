import 'dart:convert';

import '../api.dart';
import 'model/currency.dart';

class CurrencyApi extends Api {
  final String _urlString =
      "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json";

  Future<List<ApiCurrency>> getCurrencies() async {
    var response = await this.client.get(_urlString);
    if (response.statusCode == 200) {
      final body = json.decode(response.body) as List;
      return body.map((json) => ApiCurrency.fromJson(json)).toList();
    } else {
      throw Error();
    }
  }
}
