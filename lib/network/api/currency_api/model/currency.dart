import 'package:json_annotation/json_annotation.dart';

part 'currency.g.dart';

@JsonSerializable()
class ApiCurrency {
  @JsonKey(name: 'txt')
  final String name;

  final double rate;

  @JsonKey(name: 'cc')
  final String abbreviation;

  ApiCurrency(this.name, this.rate, this.abbreviation);

  factory ApiCurrency.fromJson(Map<String, dynamic> json) =>
      _$ApiCurrencyFromJson(json);

  Map<String, dynamic> toJson() => _$ApiCurrencyToJson(this);
}
