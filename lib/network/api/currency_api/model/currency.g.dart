// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'currency.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ApiCurrency _$ApiCurrencyFromJson(Map<String, dynamic> json) {
  return ApiCurrency(
    json['txt'] as String,
    (json['rate'] as num)?.toDouble(),
    json['cc'] as String,
  );
}

Map<String, dynamic> _$ApiCurrencyToJson(ApiCurrency instance) =>
    <String, dynamic>{
      'txt': instance.name,
      'rate': instance.rate,
      'cc': instance.abbreviation,
    };
