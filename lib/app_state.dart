import 'package:currancyconverter/bloc_provider.dart';
import 'package:flutter/material.dart';

class AppContainer extends StatefulWidget {
  final Widget child;
  final BlocProvider blocProvider;

  const AppContainer(
      {Key key, @required this.child, @required this.blocProvider})
      : super(key: key);

  @override
  AppContainerState createState() => AppContainerState();
}

class AppContainerState extends State<AppContainer> {
  BlocProvider get blocProvider => widget.blocProvider;

  static AppContainerState of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<AppController>().state;
  }

  @override
  Widget build(BuildContext context) {
    return AppController(widget.child, this);
  }
}

class AppController extends InheritedWidget {
  final AppContainerState state;
  final Widget child;

  AppController(this.child, this.state) : super(child: child);

  @override
  bool updateShouldNotify(AppController oldWidget) {
    return this.state != oldWidget.state;
  }
}
