import 'dart:async';

import 'package:currancyconverter/network/api/currency_api/model/currency.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

abstract class DBHelperService {
  Future<void> cleanCurrencies();

  Future<void> addCurrencies(List<ApiCurrency> currencies);

  Future<List<ApiCurrency>> getCurrencies();
}

class DBHelper implements DBHelperService {
  static const String _currencyTableName = "CURRENCY";
  static final DBHelper helper = DBHelper._();
  static Database _database;

  DBHelper._();

  Future<Database> get database async {
    if (_database == null) {
      _database = await initDatabase();
    }
    return _database;
  }

  Future<Database> initDatabase() async {
    var path = join(await getDatabasesPath(), 'currency.db');
    var database = openDatabase(path, onCreate: (db, version) {
      db.execute(
          "CREATE TABLE ${DBHelper._currencyTableName}(txt TEXT PRIMARY KEY, cc TEXT, rate NUMBER)");
    }, version: 1);
    return database;
  }

  @override
  Future<void> addCurrencies(List<ApiCurrency> currencies) async {
    var db = await database;
    var jsonCurrencies =
        currencies.map((currency) => currency.toJson()).toList();
    var batch = db.batch();
    jsonCurrencies.forEach((jsonCurrency) {
      batch.insert(DBHelper._currencyTableName, jsonCurrency);
    });
    batch.commit();
  }

  @override
  Future<void> cleanCurrencies() async {
    var db = await database;
    return db.execute("DELETE FROM ${DBHelper._currencyTableName}");
  }

  @override
  Future<List<ApiCurrency>> getCurrencies() async {
    var db = await database;
    var jsonCurrencies = await db.query(DBHelper._currencyTableName);
    var apiCurrecnies = jsonCurrencies
        .map((jsonCurrency) => ApiCurrency.fromJson(jsonCurrency))
        .toList();
    return apiCurrecnies;
  }
}
